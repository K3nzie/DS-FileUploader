-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: rs182759\-001.privatesql
-- Creato il: Mar 30, 2018 alle 11:13
-- Versione del server: 5.7.20-log
-- Versione PHP: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dsUploader`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_double_episodes`
--

CREATE TABLE `asstv_double_episodes` (
  `episode` int(11) NOT NULL,
  `season` int(11) NOT NULL,
  `id_tv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_episodes`
--

CREATE TABLE `asstv_episodes` (
  `id` int(11) NOT NULL,
  `id_tv` int(11) NOT NULL,
  `season` int(11) NOT NULL,
  `episode` int(11) NOT NULL,
  `title` varchar(512) NOT NULL DEFAULT '',
  `data` date NOT NULL,
  `creation_date` datetime NOT NULL,
  `lastmod` datetime NOT NULL,
  `online_date` datetime DEFAULT NULL,
  `stream_1080p` text,
  `stream_720p` text,
  `stream_480p` text,
  `stream_360p` text,
  `link_stream` text,
  `host_stream` text,
  `link_download` text,
  `host_download` text,
  `link_crediti` text,
  `nome_crediti` text,
  `is_online` tinyint(1) NOT NULL,
  `show_user` tinyint(1) NOT NULL DEFAULT '1',
  `visite` int(11) NOT NULL DEFAULT '0',
  `n_commenti` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_genere`
--

CREATE TABLE `asstv_genere` (
  `id` int(11) NOT NULL,
  `nome` varchar(32) NOT NULL,
  `descrizione` text NOT NULL,
  `percentuale_usi` float NOT NULL,
  `hide` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `asstv_genere`
--

INSERT INTO `asstv_genere` (`id`, `nome`, `descrizione`, `percentuale_usi`, `hide`) VALUES
(1, 'Animazione', '', 0, 1),
(4, 'Avventura', 'Anime incentrati sull\'esplorazione di nuovi luoghi, ambienti o situazioni. Questo Ã¨ spesso associato a persone che incontrano cose incredibili nei loro lunghi viaggi, descritte in un modo piuttosto avvincente e interessante.', 18.2327, 0),
(5, 'Azione', 'Anime basati principalmente su scontri di forza fisica. Spesso queste opere hanno tagli veloci, personaggi complessi che prendono decisioni rapide e di solito una bella ragazza nelle vicinanze. Una trama sottile che si sviluppa in modo rapido.', 41.4989, 0),
(7, 'Biografico', '', 0, 1),
(8, 'Comico', '', 0, 1),
(9, 'Commedia', 'Anime con piÃ¹ personaggi e/o eventi che riproducono situazioni esilaranti. Queste storie sono costruite su personaggi, situazioni ed eventi divertenti.', 52.4608, 0),
(10, 'Crimine', 'Crimine Ã¨ il termine con cui si definisce quel genere di animazione che tratta di criminalitÃ  organizzata e, piÃ¹ precisamente, di malavitosi di umili origini che nel tempo scalano sugli altri per diventare loro stessi i capi di solito, dopo la conquista del successo criminale, il protagonista subisce una rovinosa caduta.', 0.559284, 0),
(11, 'Demenziale', 'Anime basati su elementi paradossali, assurdi, eccentricamente dissacratori, che raggiungono effetti divertenti. Hanno spesso come scopo quello di raggiungere la comicitÃ  dell\'eccessiva contraddizione.', 1.78971, 0),
(13, 'Documentario', '', 0, 1),
(14, 'Dramma', 'Genere che racchiude un immensa quantitÃ  di temi: dall\'approfondimento della personalitÃ , delle emozioni e delle motivazioni alla base dei comportamenti dei personaggi e delle loro reciproche relazioni. Il dramma Ã¨ dunque un genere che presenta spesso degli aspetti intimisti, con un interesse per la descrizione psicologica dei protagonisti e per la loro evoluzione nel corso della trama.', 25.5034, 0),
(15, 'Ecchi', 'Anime che contengono molte allusioni sessuali. La traduzione di questa lettera (Ecchi Ã¨ la lettera \"H\" in giapponese) Ã¨ \"pervertito\". Questo genere parla di mutandine e di reggiseni/seni, situazioni con \"nuditÃ  improvvisa\" e, naturalmente, accenni sottili o pensieri sessuali. Negli ecchi la nuditÃ  Ã¨ quasi sempre utilizzata a scopo umoristico.', 10.6264, 0),
(16, 'Family Drama', '', 0.111857, 1),
(17, 'Fantascienza', 'Anime incentrati su tematiche antirealistiche e con una predisposizione all\'intrattenimento. Tratta non solo la realtÃ  quotidiana, ma anche le fantasie ed i sogni degli esseri umani.', 17.3378, 0),
(18, 'Fantasy', 'Anime ambientati in un mondo mitico piuttosto diverso dalla Terra moderna. Spesso questo mondo ha creature magiche e/o mitiche. Queste opere sono a volte basate su leggende e miti del mondo reale, altre su storie di magie, cavalieri coraggiosi e damigelle in pericolo.', 27.9642, 0),
(19, 'Giallo', 'La suspense Ã¨ la caratteristica principale in questo genere. La trama Ã¨ sempre costruita attorno a un mistero (generalmente un omicidio o un\'altra azione criminosa) sul quale indaga il protagonista.', 0.33557, 0),
(20, 'Militare', 'Anime che hanno un forte sentimento militaristico e nel quale i personaggi sono spesso organizzati attraverso alla comune gerarchia militare. Di solito il tema Ã¨ incentrato sulla guerra.', 1.78971, 0),
(24, 'Horror', 'Genere caratterizzato dalla presenza di scene finalizzate a suscitare nello spettatore emozioni di paura, disgusto ed orrore.', 5.7047, 0),
(26, 'Legal Drama', '', 0, 1),
(29, 'Medical Drama', '', 0, 1),
(30, 'Musica', 'Anime il cui tema centrale ruota attorno a cantanti/idol o persone che suonano strumenti. Sono spesso caratterizzati da una colonna sonora articolata.', 5.25727, 0),
(31, 'Noir', '', 0, 1),
(33, 'Poliziesco', 'Anime incentrati sulle indagini condotte da poliziotti o altri tutori della legge, o piÃ¹ in generale sulla lotta al crimine.', 1.23043, 0),
(34, 'Prison Drama', '', 0, 1),
(39, 'Sentimentale', 'Anime nei quali l\'amore Ã¨ il tema dominante. Il rapporto d\'amore tra i personaggi rappresenta uno degli aspetti fondamentali della trama.', 28.5235, 0),
(44, 'Storico', 'Anime ambientati nel passato e basati su fatti storici realmente accaduti.', 4.80984, 0),
(46, 'Teen Drama', '', 0, 1),
(47, 'Mistero', 'Anime il cui tema principale della trama Ã¨ avvolto dal mistero. Avvenimenti e personaggi rivelano segreti che generalmente portano alla risoluzione di questo.', 7.15884, 0),
(48, 'Vampiri', 'Anime in cui i vampiri sono i protagonisti o svolgono un ruolo dominante nella storia.', 2.90828, 0),
(35, 'Psicologico', 'A dominare in questo genere vi Ã¨ il mondo interiore dei personaggi, i loro processi psichici, le emozioni che derivano dal profondo, gli stati d\'animo e le riflessioni consce o inconsce.', 7.27069, 0),
(41, 'Shonen', 'Anime destinati ad un pubblico di \"ragazzi\". Gli argomenti usuali riguardano la lotta, l\'amicizia e talvolta i super poteri.', 22.9306, 0),
(49, 'Videogame', 'Anime nel quale la trama gira intorno ad uno o piÃ¹ videogiochi. Spesso i protagonisti si ritrovano all\'interno un videogioco di genere MMO o MMORPG che arriva a sostituire sotto tutti gli aspetti la loro vita reale.', 2.12528, 0),
(3, 'Automobili', '', 0, 1),
(12, 'Demoni', 'Anime ambientati in un mondo in cui i demoni e altre creature oscure svolgono un ruolo significativo. Il protagonista potrebbe anche essere uno di loro.', 5.48098, 0),
(50, 'Western', '', 0, 1),
(22, 'Harem', 'Anime nei quali un gruppo femminile accompagna, ed in alcuni casi convive, con un singolo ragazzo. Quando si presenta un rapporto amoroso, questo Ã¨ spesso condiviso da piÃ¹ di 2 persone.', 9.17226, 0),
(23, 'Hentai', 'Anime con contenuti sessualmente espliciti. Ha una trama di solito molto piccola incentrata sul sesso, paragonabile al porno normale. Gli hentai possono spaziare dalla relazione sessuale della coppia normale a una vasta gamma di feticismi e perversioni.', 0.447427, 0),
(25, 'Josei', 'Anime dedicati alle donne giovani e adulte, corrispettivo femminile di seinen.', 1.34228, 0),
(2, 'Arti Marziali', 'Anime il cui tema centrale ruota attorno alle arti marziali. CiÃ² include tutti gli stili di combattimento corpo a corpo, tra cui Karate, Tae-Kwon-Do e persino Boxe. Anche alcune armi, come Nunchaku e Shuriken, sono collegate di questo genere.', 1.78971, 0),
(28, 'Mecha', 'Anime il cui tema centrale riguarda comparti meccanici. Questo genere Ã¨ usato principalmente per indicare la presenza robot giganti.', 6.4877, 0),
(27, 'Magia', 'Anime il cui tema centrale ruota intorno alla magia. Accadono cose che sono fuori dall\'ordinario e non possono essere spiegate dalle leggi della natura o della scienza.', 9.61969, 0),
(6, 'Bambini', 'Anime il cui target di riferimento sono i bambini. Questo non significa necessariamente che i personaggi principali siano bambini.', 1.78971, 0),
(21, 'Spazio', 'Anime la cui ambientazione Ã¨ nello spazio esterno, non su un altro pianeta, nÃ© in un\'altra dimensione, ma nello spazio.', 3.13199, 0),
(32, 'Parodia', 'Anime che imitano altre storie (anime, serie TV, film, libri, eventi storici) esagerando lo stile e cambiando il contenuto dell\'originale. Utilizzano spesso la tecnica della satira.', 2.79642, 0),
(36, 'Samurai', 'Anime che hanno come protagonista un samurai (guerrieri medievali giapponesi).', 1.23043, 0),
(37, 'Scuola', 'Anime che sono ambientati principalmente in una scuola. Il tema ruota intorno alla vita di adoloscenti. E\' spesso un sinonimo di Teen Drama.', 29.3065, 0),
(40, 'Shojo', 'Anime destinati ad un pubblico di \"ragazze\". Di solito la storia Ã¨ narrata dal punto di vista di una ragazza e si occupa di romanticismo, dramma o magia.', 6.26398, 0),
(38, 'Seinen', 'Anime che hanno come target un pubblico maschile che va dalla maggiore etÃ  in su.', 12.4161, 0),
(45, 'Superpoteri', 'Anime nel quale i protagonisti hanno poteri sovrumani. Spesso sembra magico, ma non puÃ² essere considerato davvero tale di solito si tratta di attacchi di ki, capacitÃ  di volare o forza sovrumana.', 8.05369, 0),
(43, 'Sport', 'Anime che ha lo sport (tennis, basket, calcio, baseball, pallavolo...) come tema centrale.', 7.15884, 0),
(42, 'Sovrannaturale', 'Anime incentrato sul paranormale. Demoni, vampiri, fantasmi, zombie, etc...', 25.2796, 0),
(52, 'Slice of Life', 'Anime senza una trama centrale nitida che si incentrano in un particolare momento della vita dei personaggi. Questo tipo di anime tende ad essere realistico e a raccontare storie di vita quotidiana.', 3.91499, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_groups`
--

CREATE TABLE `asstv_groups` (
  `id` int(11) NOT NULL,
  `order_view` int(11) NOT NULL DEFAULT '-1',
  `list_show` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(64) NOT NULL DEFAULT '',
  `descrizione` text,
  `god` tinyint(1) NOT NULL DEFAULT '0',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `add_tv` tinyint(1) NOT NULL DEFAULT '0',
  `modify_tv` tinyint(1) NOT NULL DEFAULT '0',
  `global_tv_mod` tinyint(1) NOT NULL DEFAULT '0',
  `mod_forum` tinyint(1) DEFAULT '0',
  `global_forum_mod` tinyint(1) NOT NULL DEFAULT '0',
  `mod_chat` text,
  `post` tinyint(1) NOT NULL DEFAULT '1',
  `ban` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `asstv_groups`
--

INSERT INTO `asstv_groups` (`id`, `order_view`, `list_show`, `name`, `descrizione`, `god`, `admin`, `add_tv`, `modify_tv`, `global_tv_mod`, `mod_forum`, `global_forum_mod`, `mod_chat`, `post`, `ban`) VALUES
(1, 0, 1, 'Admin', NULL, 1, 1, 1, 1, 1, 1, 1, '1', 1, 0),
(2, 6, 1, 'Utente', NULL, 0, 0, 0, 0, 0, 0, 0, '0', 1, 0),
(3, 7, 0, 'Da Convalidare', NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 0),
(4, 8, 1, 'Bannato', NULL, 0, 0, 0, 0, 0, 0, 0, '0', 0, 1),
(5, 1, 1, 'Supervisore', NULL, 0, 1, 1, 1, 1, 1, 1, '1', 1, 0),
(6, 5, 1, 'Grafico', NULL, 0, 0, 0, 0, 0, 1, 0, '1', 1, 0),
(7, 4, 1, 'Sviluppatore', NULL, 0, 0, 0, 0, 0, 1, 0, '1', 1, 0),
(8, 2, 1, 'Releaser', NULL, 0, 1, 1, 1, 0, 1, 0, '1', 1, 0),
(9, 3, 1, 'Moderatore', NULL, 0, 0, 0, 0, 0, 1, 1, '1', 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_group_assign`
--

CREATE TABLE `asstv_group_assign` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_notifiche`
--

CREATE TABLE `asstv_notifiche` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `clean` varchar(256) DEFAULT NULL,
  `id_ep` int(11) DEFAULT NULL,
  `id_msg` int(11) DEFAULT NULL,
  `type` enum('new_ep','pub_msg','prv_msg','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_paese`
--

CREATE TABLE `asstv_paese` (
  `id` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(128) DEFAULT NULL,
  `sigla_numerica_stati` varchar(4) DEFAULT NULL,
  `sigla_iso_3166_1_alpha_3_stati` varchar(3) DEFAULT NULL,
  `sigla_iso_3166_1_alpha_2_stati` varchar(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `asstv_paese`
--

INSERT INTO `asstv_paese` (`id`, `nome`, `sigla_numerica_stati`, `sigla_iso_3166_1_alpha_3_stati`, `sigla_iso_3166_1_alpha_2_stati`) VALUES
(1, 'Afghanistan', '004', 'AFG', 'AF'),
(2, 'Albania', '008', 'ALB', 'AL'),
(3, 'Algeria', '012', 'DZA', 'DZ'),
(4, 'Andorra', '020', 'AND', 'AD'),
(5, 'Angola', '024', 'AGO', 'AO'),
(6, 'Anguilla', '660', 'AIA', 'AI'),
(7, 'Antartide', '010', 'ATA', 'AQ'),
(8, 'Antigua e Barbuda', '028', 'ATG', 'AG'),
(9, 'Arabia Saudita', '682', 'SAU', 'SA'),
(10, 'Argentina', '032', 'ARG', 'AR'),
(11, 'Armenia', '051', 'ARM', 'AM'),
(12, 'Aruba', '533', 'ABW', 'AW'),
(13, 'Australia', '036', 'AUS', 'AU'),
(14, 'Austria', '040', 'AUT', 'AT'),
(15, 'Azerbaigian', '031', 'AZE', 'AZ'),
(16, 'Bahamas', '044', 'BHS', 'BS'),
(17, 'Bahrein', '048', 'BHR', 'BH'),
(18, 'Bangladesh', '050', 'BGD', 'BD'),
(19, 'Barbados', '052', 'BRB', 'BB'),
(20, 'Belgio', '056', 'BEL', 'BE'),
(21, 'Belize', '084', 'BLZ', 'BZ'),
(22, 'Benin', '204', 'BEN', 'BJ'),
(23, 'Bermuda', '060', 'BMU', 'BM'),
(24, 'Bhutan', '064', 'BTN', 'BT'),
(25, 'Bielorussia', '112', 'BLR', 'BY'),
(26, 'Birmania', '104', 'MMR', 'MM'),
(27, 'Bolivia', '068', 'BOL', 'BO'),
(28, 'Bosnia ed Erzegovina', '070', 'BIH', 'BA'),
(29, 'Botswana', '072', 'BWA', 'BW'),
(30, 'Brasile', '076', 'BRA', 'BR'),
(31, 'Brunei', '096', 'BRN', 'BN'),
(32, 'Bulgaria', '100', 'BGR', 'BG'),
(33, 'Burkina Faso', '854', 'BFA', 'BF'),
(34, 'Burundi', '108', 'BDI', 'BI'),
(35, 'Cambogia', '116', 'KHM', 'KH'),
(36, 'Camerun', '120', 'CMR', 'CM'),
(37, 'Canada', '124', 'CAN', 'CA'),
(38, 'Capo Verde', '132', 'CPV', 'CV'),
(39, 'Ciad', '148', 'TCD', 'TD'),
(40, 'Cile', '152', 'CHL', 'CL'),
(41, 'Cina', '156', 'CHN', 'CN'),
(42, 'Cipro', '196', 'CYP', 'CY'),
(43, 'Citt', '336', 'VAT', 'VA'),
(44, 'Colombia', '170', 'COL', 'CO'),
(45, 'Comore', '174', 'COM', 'KM'),
(46, 'Corea del Nord', '408', 'PRK', 'KP'),
(47, 'Corea del Sud', '410', 'KOR', 'KR'),
(48, 'Costa d\'Avorio', '384', 'CIV', 'CI'),
(49, 'Costa Rica', '188', 'CRI', 'CR'),
(50, 'Croazia', '191', 'HRV', 'HR'),
(51, 'Cuba', '192', 'CUB', 'CU'),
(52, 'Cura', '531', 'CUW', 'CW'),
(53, 'Danimarca', '208', 'DNK', 'DK'),
(54, 'Dominica', '212', 'DMA', 'DM'),
(55, 'Ecuador', '218', 'ECU', 'EC'),
(56, 'Egitto', '818', 'EGY', 'EG'),
(57, 'El Salvador', '222', 'SLV', 'SV'),
(58, 'Emirati Arabi Uniti', '784', 'ARE', 'AE'),
(59, 'Eritrea', '232', 'ERI', 'ER'),
(60, 'Estonia', '233', 'EST', 'EE'),
(61, 'Etiopia', '231', 'ETH', 'ET'),
(62, 'Figi', '242', 'FJI', 'FJ'),
(63, 'Filippine', '608', 'PHL', 'PH'),
(64, 'Finlandia', '246', 'FIN', 'FI'),
(65, 'Francia', '250', 'FRA', 'FR'),
(66, 'Gabon', '266', 'GAB', 'GA'),
(67, 'Gambia', '270', 'GMB', 'GM'),
(68, 'Georgia', '268', 'GEO', 'GE'),
(69, 'Georgia del Sud e isole Sandwich meridionali', '239', 'SGS', 'GS'),
(70, 'Germania', '276', 'DEU', 'DE'),
(71, 'Ghana', '288', 'GHA', 'GH'),
(72, 'Giamaica', '388', 'JAM', 'JM'),
(73, 'Giappone', '392', 'JPN', 'JP'),
(74, 'Gibilterra', '292', 'GIB', 'GI'),
(75, 'Gibuti', '262', 'DJI', 'DJ'),
(76, 'Giordania', '400', 'JOR', 'JO'),
(77, 'Grecia', '300', 'GRC', 'GR'),
(78, 'Grenada', '308', 'GRD', 'GD'),
(79, 'Groenlandia', '304', 'GRL', 'GL'),
(80, 'Guadalupa', '312', 'GLP', 'GP'),
(81, 'Guam', '316', 'GUM', 'GU'),
(82, 'Guatemala', '320', 'GTM', 'GT'),
(83, 'Guernsey', '831', 'GGY', 'GG'),
(84, 'Guinea', '324', 'GIN', 'GN'),
(85, 'Guinea-Bissau', '624', 'GNB', 'GW'),
(86, 'Guinea Equatoriale', '226', 'GNQ', 'GQ'),
(87, 'Guyana', '328', 'GUY', 'GY'),
(88, 'Guyana francese', '254', 'GUF', 'GF'),
(89, 'Haiti', '332', 'HTI', 'HT'),
(90, 'Honduras', '340', 'HND', 'HN'),
(91, 'Hong Kong', '344', 'HKG', 'HK'),
(92, 'India', '356', 'IND', 'IN'),
(93, 'Indonesia', '360', 'IDN', 'ID'),
(94, 'Iran', '364', 'IRN', 'IR'),
(95, 'Iraq', '368', 'IRQ', 'IQ'),
(96, 'Irlanda', '372', 'IRL', 'IE'),
(97, 'Islanda', '352', 'ISL', 'IS'),
(98, 'Isola Bouvet', '074', 'BVT', 'BV'),
(99, 'Isola di Man', '833', 'IMN', 'IM'),
(100, 'Isola di Natale', '162', 'CXR', 'CX'),
(101, 'Isola Norfolk', '574', 'NFK', 'NF'),
(102, 'Isole ', '248', 'ALA', 'AX'),
(103, 'Isole BES', '535', 'BES', 'BQ'),
(104, 'Isole Cayman', '136', 'CYM', 'KY'),
(105, 'Isole Cocos (Keeling)', '166', 'CCK', 'CC'),
(106, 'Isole Cook', '184', 'COK', 'CK'),
(107, 'Isole Foroyar', '234', 'FRO', 'FO'),
(108, 'Isole Falkland', '238', 'FLK', 'FK'),
(109, 'Isole Heard e McDonald', '334', 'HMD', 'HM'),
(110, 'Isole Marianne Settentrionali', '580', 'MNP', 'MP'),
(111, 'Isole Marshall', '584', 'MHL', 'MH'),
(112, 'Isole minori esterne degli Stati Uniti', '581', 'UMI', 'UM'),
(113, 'Isole Pitcairn', '612', 'PCN', 'PN'),
(114, 'Isole Salomone', '090', 'SLB', 'SB'),
(115, 'Isole Vergini britanniche', '092', 'VGB', 'VG'),
(116, 'Isole Vergini americane', '850', 'VIR', 'VI'),
(117, 'Israele', '376', 'ISR', 'IL'),
(118, 'Italia', '380', 'ITA', 'IT'),
(119, 'Jersey', '832', 'JEY', 'JE'),
(120, 'Kazakistan', '398', 'KAZ', 'KZ'),
(121, 'Kenya', '404', 'KEN', 'KE'),
(122, 'Kirghizistan', '417', 'KGZ', 'KG'),
(123, 'Kiribati', '296', 'KIR', 'KI'),
(124, 'Kuwait', '414', 'KWT', 'KW'),
(125, 'Laos', '418', 'LAO', 'LA'),
(126, 'Lesotho', '426', 'LSO', 'LS'),
(127, 'Lettonia', '428', 'LVA', 'LV'),
(128, 'Libano', '422', 'LBN', 'LB'),
(129, 'Liberia', '430', 'LBR', 'LR'),
(130, 'Libia', '434', 'LBY', 'LY'),
(131, 'Liechtenstein', '438', 'LIE', 'LI'),
(132, 'Lituania', '440', 'LTU', 'LT'),
(133, 'Lussemburgo', '442', 'LUX', 'LU'),
(134, 'Macao', '446', 'MAC', 'MO'),
(135, 'Macedonia', '807', 'MKD', 'MK'),
(136, 'Madagascar', '450', 'MDG', 'MG'),
(137, 'Malawi', '454', 'MWI', 'MW'),
(138, 'Malesia', '458', 'MYS', 'MY'),
(139, 'Maldive', '462', 'MDV', 'MV'),
(140, 'Mali', '466', 'MLI', 'ML'),
(141, 'Malta', '470', 'MLT', 'MT'),
(142, 'Marocco', '504', 'MAR', 'MA'),
(143, 'Martinica', '474', 'MTQ', 'MQ'),
(144, 'Mauritania', '478', 'MRT', 'MR'),
(145, 'Mauritius', '480', 'MUS', 'MU'),
(146, 'Mayotte', '175', 'MYT', 'YT'),
(147, 'Messico', '484', 'MEX', 'MX'),
(148, 'Micronesia', '583', 'FSM', 'FM'),
(149, 'Moldavia', '498', 'MDA', 'MD'),
(150, 'Mongolia', '496', 'MNG', 'MN'),
(151, 'Montenegro', '499', 'MNE', 'ME'),
(152, 'Montserrat', '500', 'MSR', 'MS'),
(153, 'Mozambico', '508', 'MOZ', 'MZ'),
(154, 'Namibia', '516', 'NAM', 'NA'),
(155, 'Nauru', '520', 'NRU', 'NR'),
(156, 'Nepal', '524', 'NPL', 'NP'),
(157, 'Nicaragua', '558', 'NIC', 'NI'),
(158, 'Niger', '562', 'NER', 'NE'),
(159, 'Nigeria', '566', 'NGA', 'NG'),
(160, 'Niue', '570', 'NIU', 'NU'),
(161, 'Norvegia', '578', 'NOR', 'NO'),
(162, 'Nuova Caledonia', '540', 'NCL', 'NC'),
(163, 'Nuova Zelanda', '554', 'NZL', 'NZ'),
(164, 'Oman', '512', 'OMN', 'OM'),
(165, 'Paesi Bassi', '528', 'NLD', 'NL'),
(166, 'Pakistan', '586', 'PAK', 'PK'),
(167, 'Palau', '585', 'PLW', 'PW'),
(168, 'Palestina', '275', 'PSE', 'PS'),
(169, 'Panam', '591', 'PAN', 'PA'),
(170, 'Papua Nuova Guinea', '598', 'PNG', 'PG'),
(171, 'Paraguay', '600', 'PRY', 'PY'),
(172, 'Per', '604', 'PER', 'PE'),
(173, 'Polinesia Francese', '258', 'PYF', 'PF'),
(174, 'Polonia', '616', 'POL', 'PL'),
(175, 'Porto Rico', '630', 'PRI', 'PR'),
(176, 'Portogallo', '620', 'PRT', 'PT'),
(177, 'Monaco', '492', 'MCO', 'MC'),
(178, 'Qatar', '634', 'QAT', 'QA'),
(179, 'Regno Unito', '826', 'GBR', 'GB'),
(180, 'RD del Congo', '180', 'COD', 'CD'),
(181, 'Rep. Ceca', '203', 'CZE', 'CZ'),
(182, 'Rep. Centrafricana', '140', 'CAF', 'CF'),
(183, 'Rep. del Congo', '178', 'COG', 'CG'),
(184, 'Rep. Dominicana', '214', 'DOM', 'DO'),
(185, 'Riunione', '638', 'REU', 'RE'),
(186, 'Romania', '642', 'ROU', 'RO'),
(187, 'Ruanda', '646', 'RWA', 'RW'),
(188, 'Russia', '643', 'RUS', 'RU'),
(189, 'Sahara Occidentale', '732', 'ESH', 'EH'),
(190, 'Saint Kitts e Nevis', '659', 'KNA', 'KN'),
(191, 'Santa Lucia', '662', 'LCA', 'LC'),
(192, 'Sant\'Elena, Ascensione e Tristan da Cunha', '654', 'SHN', 'SH'),
(193, 'Saint Vincent e Grenadine', '670', 'VCT', 'VC'),
(194, 'Saint-Barth', '652', 'BLM', 'BL'),
(195, 'Saint-Martin', '663', 'MAF', 'MF'),
(196, 'Saint-Pierre e Miquelon', '666', 'SPM', 'PM'),
(197, 'Samoa', '882', 'WSM', 'WS'),
(198, 'Samoa Americane', '016', 'ASM', 'AS'),
(199, 'San Marino', '674', 'SMR', 'SM'),
(200, 'Sao Tome e Principe', '678', 'STP', 'ST'),
(201, 'Senegal', '686', 'SEN', 'SN'),
(202, 'Serbia', '688', 'SRB', 'RS'),
(203, 'Seychelles', '690', 'SYC', 'SC'),
(204, 'Sierra Leone', '694', 'SLE', 'SL'),
(205, 'Singapore', '702', 'SGP', 'SG'),
(206, 'Sint Maarten', '534', 'SXM', 'SX'),
(207, 'Siria', '760', 'SYR', 'SY'),
(208, 'Slovacchia', '703', 'SVK', 'SK'),
(209, 'Slovenia', '705', 'SVN', 'SI'),
(210, 'Somalia', '706', 'SOM', 'SO'),
(211, 'Spagna', '724', 'ESP', 'ES'),
(212, 'Sri Lanka', '144', 'LKA', 'LK'),
(213, 'Stati Uniti', '840', 'USA', 'US'),
(214, 'Sudafrica', '710', 'ZAF', 'ZA'),
(215, 'Sudan', '729', 'SDN', 'SD'),
(216, 'Sudan del Sud', '728', 'SSD', 'SS'),
(217, 'Suriname', '740', 'SUR', 'SR'),
(218, 'Svalbard e Jan Mayen', '744', 'SJM', 'SJ'),
(219, 'Svezia', '752', 'SWE', 'SE'),
(220, 'Svizzera', '756', 'CHE', 'CH'),
(221, 'Swaziland', '748', 'SWZ', 'SZ'),
(222, 'Taiwan', '158', 'TWN', 'TW'),
(223, 'Tagikistan', '762', 'TJK', 'TJ'),
(224, 'Tanzania', '834', 'TZA', 'TZ'),
(225, 'Terre australi e antartiche francesi', '260', 'ATF', 'TF'),
(226, 'Territorio britannico dell\'oceano Indiano', '086', 'IOT', 'IO'),
(227, 'Thailandia', '764', 'THA', 'TH'),
(228, 'Timor Est', '626', 'TLS', 'TL'),
(229, 'Togo', '768', 'TGO', 'TG'),
(230, 'Tokelau', '772', 'TKL', 'TK'),
(231, 'Tonga', '776', 'TON', 'TO'),
(232, 'Trinidad e Tobago', '780', 'TTO', 'TT'),
(233, 'Tunisia', '788', 'TUN', 'TN'),
(234, 'Turchia', '792', 'TUR', 'TR'),
(235, 'Turkmenistan', '795', 'TKM', 'TM'),
(236, 'Turks e Caicos', '796', 'TCA', 'TC'),
(237, 'Tuvalu', '798', 'TUV', 'TV'),
(238, 'Ucraina', '804', 'UKR', 'UA'),
(239, 'Uganda', '800', 'UGA', 'UG'),
(240, 'Ungheria', '348', 'HUN', 'HU'),
(241, 'Uruguay', '858', 'URY', 'UY'),
(242, 'Uzbekistan', '860', 'UZB', 'UZ'),
(243, 'Vanuatu', '548', 'VUT', 'VU'),
(244, 'Venezuela', '862', 'VEN', 'VE'),
(245, 'Vietnam', '704', 'VNM', 'VN'),
(246, 'Wallis e Futuna', '876', 'WLF', 'WF'),
(247, 'Yemen', '887', 'YEM', 'YE'),
(248, 'Zambia', '894', 'ZMB', 'ZM'),
(249, 'Zimbabwe', '716', 'ZWE', 'ZW');

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_preferite`
--

CREATE TABLE `asstv_preferite` (
  `clean` varchar(96) NOT NULL,
  `username` varchar(32) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `voto` int(11) NOT NULL DEFAULT '0',
  `stato` enum('noFollow','watched','watching','planToWatch') NOT NULL DEFAULT 'noFollow',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_restricted_permission`
--

CREATE TABLE `asstv_restricted_permission` (
  `id` int(11) NOT NULL,
  `type` enum('tv','forum') NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_tv` int(11) DEFAULT NULL,
  `id_forum` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_session`
--

CREATE TABLE `asstv_session` (
  `id` int(11) NOT NULL,
  `session_hash` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_title`
--

CREATE TABLE `asstv_title` (
  `id` int(11) NOT NULL,
  `id_tv` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `language` varchar(24) DEFAULT NULL,
  `original` tinyint(1) NOT NULL DEFAULT '0',
  `short` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_tv_series`
--

CREATE TABLE `asstv_tv_series` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `clean` varchar(256) NOT NULL,
  `copy` varchar(8) DEFAULT NULL,
  `old_copy` varchar(56) DEFAULT NULL,
  `copy_ep` tinyint(1) NOT NULL DEFAULT '0',
  `trama` text NOT NULL,
  `is_anime` tinyint(1) NOT NULL,
  `type` enum('tv','spinoff','oav','movie') NOT NULL DEFAULT 'tv',
  `n_season` int(11) NOT NULL,
  `nome_saga` text,
  `n_episode` int(11) NOT NULL,
  `id_last_episode` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `date_start` date NOT NULL,
  `week_day` tinyint(4) NOT NULL,
  `n_genere` int(11) NOT NULL,
  `id_genere` text NOT NULL,
  `n_paesi` int(11) NOT NULL,
  `id_paese` text NOT NULL,
  `durata` int(11) NOT NULL,
  `show_user` tinyint(1) NOT NULL DEFAULT '1',
  `completa` tinyint(1) NOT NULL,
  `is_online` tinyint(1) NOT NULL,
  `host` text NOT NULL,
  `language` varchar(128) NOT NULL,
  `n_review` int(11) NOT NULL,
  `trailer` text NOT NULL,
  `n_trailer` int(11) NOT NULL,
  `n_post` int(11) NOT NULL,
  `n_forum` int(11) NOT NULL,
  `n_voti` int(11) NOT NULL,
  `tot_voti` float NOT NULL,
  `visite_totali` int(11) NOT NULL DEFAULT '0',
  `visite_settimanali` int(11) NOT NULL DEFAULT '0',
  `week_day_visit` varchar(96) NOT NULL DEFAULT '| 0 | 0 | 0 | 0 | 0 | 0 | 0 |',
  `popolarita` double NOT NULL DEFAULT '0',
  `affiliate` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_user`
--

CREATE TABLE `asstv_user` (
  `id` int(11) NOT NULL,
  `username` varchar(16) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(12) NOT NULL,
  `attempt` int(11) NOT NULL DEFAULT '0',
  `mail` varchar(64) NOT NULL,
  `confirmed` varchar(32) DEFAULT NULL,
  `id_groups` text,
  `avatar` text,
  `yy_nascita` int(11) DEFAULT NULL,
  `mm_nascita` int(11) DEFAULT NULL,
  `dd_nascita` int(11) DEFAULT NULL,
  `provenienza` varchar(128) DEFAULT NULL,
  `telefono` varchar(16) DEFAULT NULL,
  `interessi` varchar(256) DEFAULT NULL,
  `sesso` enum('M','F') DEFAULT NULL,
  `anonimo` tinyint(1) NOT NULL DEFAULT '0',
  `preferite_anonimo` tinyint(1) NOT NULL DEFAULT '0',
  `stream_predefinito` enum('1080p','720p','480p','360p') NOT NULL DEFAULT '480p',
  `reg_date` timestamp NOT NULL,
  `last_access` timestamp NOT NULL,
  `n_msg` int(11) NOT NULL DEFAULT '0',
  `n_like` int(11) NOT NULL DEFAULT '0',
  `n_chat_msg` int(11) NOT NULL DEFAULT '0',
  `n_review` int(11) NOT NULL DEFAULT '0',
  `seconds_online` int(11) NOT NULL DEFAULT '0',
  `fuso_orario` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `asstv_video`
--

CREATE TABLE `asstv_video` (
  `id` int(11) NOT NULL,
  `id_tv` int(11) NOT NULL,
  `season` int(11) NOT NULL,
  `episode` int(11) NOT NULL,
  `url` text NOT NULL,
  `host` varchar(24) DEFAULT NULL,
  `stream` tinyint(1) NOT NULL DEFAULT '0',
  `download` tinyint(1) NOT NULL DEFAULT '1',
  `short` varchar(16) DEFAULT NULL,
  `quality` enum('1080p','720p','480p','360p') DEFAULT NULL,
  `language` varchar(16) DEFAULT NULL,
  `sub_language` varchar(16) DEFAULT NULL,
  `uncensored` tinyint(1) DEFAULT NULL,
  `distribution` enum('tv','web') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `recupero_password`
--

CREATE TABLE `recupero_password` (
  `mail` varchar(40) NOT NULL,
  `codice` varchar(32) NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT '0',
  `data` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `to_move_list`
--

CREATE TABLE `to_move_list` (
  `id` int(11) NOT NULL,
  `id_tv` int(11) NOT NULL,
  `episode` int(11) NOT NULL,
  `url` text,
  `language` varchar(16) NOT NULL DEFAULT 'SUB_ITA',
  `trc` tinyint(1) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '1',
  `add_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `vm_ip` varchar(16) DEFAULT NULL,
  `dropped` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `asstv_double_episodes`
--
ALTER TABLE `asstv_double_episodes`
  ADD PRIMARY KEY (`episode`,`season`,`id_tv`),
  ADD KEY `episode` (`episode`),
  ADD KEY `season` (`season`),
  ADD KEY `id_tv` (`id_tv`),
  ADD KEY `episode_2` (`episode`,`season`);

--
-- Indici per le tabelle `asstv_episodes`
--
ALTER TABLE `asstv_episodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_tv_2` (`id_tv`,`season`,`episode`),
  ADD KEY `episode` (`episode`),
  ADD KEY `season` (`season`),
  ADD KEY `id_tv` (`id_tv`),
  ADD KEY `show_user` (`show_user`),
  ADD KEY `data` (`data`),
  ADD KEY `is_online` (`is_online`),
  ADD KEY `season_3` (`season`,`episode`),
  ADD KEY `id` (`id`,`id_tv`,`season`),
  ADD KEY `online_date` (`online_date`);

--
-- Indici per le tabelle `asstv_genere`
--
ALTER TABLE `asstv_genere`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome` (`nome`),
  ADD KEY `id` (`id`);

--
-- Indici per le tabelle `asstv_groups`
--
ALTER TABLE `asstv_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `order_view` (`order_view`),
  ADD KEY `list_show` (`list_show`);

--
-- Indici per le tabelle `asstv_group_assign`
--
ALTER TABLE `asstv_group_assign`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_group` (`id_group`),
  ADD KEY `id_user` (`id_user`);

--
-- Indici per le tabelle `asstv_notifiche`
--
ALTER TABLE `asstv_notifiche`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `data` (`data`),
  ADD KEY `id_ep` (`id_ep`);

--
-- Indici per le tabelle `asstv_paese`
--
ALTER TABLE `asstv_paese`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome` (`nome`),
  ADD KEY `nome_2` (`nome`);

--
-- Indici per le tabelle `asstv_preferite`
--
ALTER TABLE `asstv_preferite`
  ADD PRIMARY KEY (`clean`,`username`),
  ADD KEY `voto` (`voto`),
  ADD KEY `stato` (`stato`),
  ADD KEY `date` (`date`),
  ADD KEY `username` (`username`);

--
-- Indici per le tabelle `asstv_restricted_permission`
--
ALTER TABLE `asstv_restricted_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_tv` (`id_tv`);

--
-- Indici per le tabelle `asstv_session`
--
ALTER TABLE `asstv_session`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `session_hash` (`session_hash`),
  ADD KEY `id_2` (`id`),
  ADD KEY `session_hash_2` (`session_hash`);

--
-- Indici per le tabelle `asstv_title`
--
ALTER TABLE `asstv_title`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tv` (`id_tv`);

--
-- Indici per le tabelle `asstv_tv_series`
--
ALTER TABLE `asstv_tv_series`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clean_2` (`clean`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `clean` (`clean`),
  ADD KEY `is_anime` (`is_anime`),
  ADD KEY `week_day` (`week_day`),
  ADD KEY `name_2` (`name`),
  ADD KEY `n_episode` (`n_episode`),
  ADD KEY `show_user` (`show_user`),
  ADD KEY `popolarita` (`popolarita`),
  ADD KEY `completa` (`completa`),
  ADD KEY `type` (`type`),
  ADD FULLTEXT KEY `name` (`name`),
  ADD FULLTEXT KEY `trama` (`trama`);

--
-- Indici per le tabelle `asstv_user`
--
ALTER TABLE `asstv_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail_2` (`mail`),
  ADD UNIQUE KEY `username_2` (`username`),
  ADD KEY `username` (`username`),
  ADD KEY `mail` (`mail`);

--
-- Indici per le tabelle `asstv_video`
--
ALTER TABLE `asstv_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tv` (`id_tv`),
  ADD KEY `episode` (`episode`),
  ADD KEY `season` (`season`),
  ADD KEY `stream` (`stream`),
  ADD KEY `download` (`download`),
  ADD KEY `quality` (`quality`),
  ADD KEY `sub_language` (`sub_language`),
  ADD KEY `language` (`language`);

--
-- Indici per le tabelle `to_move_list`
--
ALTER TABLE `to_move_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tv` (`id_tv`),
  ADD KEY `episode` (`episode`),
  ADD KEY `priority` (`priority`),
  ADD KEY `vm_ip` (`vm_ip`),
  ADD KEY `end_date` (`end_date`),
  ADD KEY `add_date` (`add_date`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `asstv_episodes`
--
ALTER TABLE `asstv_episodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43838;
--
-- AUTO_INCREMENT per la tabella `asstv_genere`
--
ALTER TABLE `asstv_genere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT per la tabella `asstv_groups`
--
ALTER TABLE `asstv_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT per la tabella `asstv_group_assign`
--
ALTER TABLE `asstv_group_assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4588;
--
-- AUTO_INCREMENT per la tabella `asstv_notifiche`
--
ALTER TABLE `asstv_notifiche`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28216;
--
-- AUTO_INCREMENT per la tabella `asstv_restricted_permission`
--
ALTER TABLE `asstv_restricted_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=374;
--
-- AUTO_INCREMENT per la tabella `asstv_session`
--
ALTER TABLE `asstv_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3700;
--
-- AUTO_INCREMENT per la tabella `asstv_title`
--
ALTER TABLE `asstv_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1913;
--
-- AUTO_INCREMENT per la tabella `asstv_tv_series`
--
ALTER TABLE `asstv_tv_series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1436;
--
-- AUTO_INCREMENT per la tabella `asstv_user`
--
ALTER TABLE `asstv_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4579;
--
-- AUTO_INCREMENT per la tabella `asstv_video`
--
ALTER TABLE `asstv_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206919;
--
-- AUTO_INCREMENT per la tabella `to_move_list`
--
ALTER TABLE `to_move_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28264;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `asstv_episodes`
--
ALTER TABLE `asstv_episodes`
  ADD CONSTRAINT `asstv_episodes_ibfk_1` FOREIGN KEY (`id_tv`) REFERENCES `asstv_tv_series` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `asstv_group_assign`
--
ALTER TABLE `asstv_group_assign`
  ADD CONSTRAINT `asstv_group_assign_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `asstv_groups` (`id`),
  ADD CONSTRAINT `asstv_group_assign_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `asstv_user` (`id`);

--
-- Limiti per la tabella `asstv_notifiche`
--
ALTER TABLE `asstv_notifiche`
  ADD CONSTRAINT `asstv_notifiche_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `asstv_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `asstv_notifiche_ibfk_2` FOREIGN KEY (`id_ep`) REFERENCES `asstv_episodes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `asstv_preferite`
--
ALTER TABLE `asstv_preferite`
  ADD CONSTRAINT `asstv_preferite_ibfk_1` FOREIGN KEY (`clean`) REFERENCES `asstv_tv_series` (`clean`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `asstv_preferite_ibfk_2` FOREIGN KEY (`username`) REFERENCES `asstv_user` (`username`);

--
-- Limiti per la tabella `asstv_restricted_permission`
--
ALTER TABLE `asstv_restricted_permission`
  ADD CONSTRAINT `asstv_restricted_permission_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `asstv_user` (`id`),
  ADD CONSTRAINT `asstv_restricted_permission_ibfk_2` FOREIGN KEY (`id_tv`) REFERENCES `asstv_tv_series` (`id`);

--
-- Limiti per la tabella `asstv_title`
--
ALTER TABLE `asstv_title`
  ADD CONSTRAINT `asstv_title_ibfk_1` FOREIGN KEY (`id_tv`) REFERENCES `asstv_tv_series` (`id`);

--
-- Limiti per la tabella `asstv_video`
--
ALTER TABLE `asstv_video`
  ADD CONSTRAINT `asstv_video_ibfk_1` FOREIGN KEY (`id_tv`) REFERENCES `asstv_tv_series` (`id`),
  ADD CONSTRAINT `asstv_video_ibfk_2` FOREIGN KEY (`episode`) REFERENCES `asstv_episodes` (`episode`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
