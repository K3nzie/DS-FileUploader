var uploader = new Vue({
  el: '#uploadApp',
  data: {
  	dragDropCapable: false,
  	//dropSpan: "Trascita il file in quest'area per cominciare l'upload!",
  	//isOnEnter: false,
    //files: [],
    file: null,
    size: null,
    //fileIcon: "data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4IiB2aWV3Qm94PSIwIDAgNTYuMjUgNTYuMjUiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDU2LjI1IDU2LjI1OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTU2LjI1LDM5LjE0NXYtMS4wNDJWMTAuMzI1YzAtMi4zMDEtMS44NjUtNC4xNjctNC4xNjctNC4xNjdINC4xNjdDMS44NjYsNi4xNTgsMCw4LjAyMywwLDEwLjMyNXYyNy43Nzh2MS4wNDJ2MTAuOTQ3ICAgIGg1Ni4yNVYzOS4xNDV6IE0yMS4wNTcsMTguNjZjMC0yLjMwMSwxLjY2OS0zLjMzMywzLjcyNy0yLjMwM2w4Ljg2NCw0LjQzMmMyLjA2LDEuMDI5LDIuMDYsMi42OTcsMCwzLjcyNmwtOC44NjQsNC40MzMgICAgYy0yLjA1OCwxLjAyOC0zLjcyNy0wLjAwMi0zLjcyNy0yLjMwM1YxOC42NnogTTU0LjE5NSw0OC4wMDlIMi4wNTR2LTcuODIzaDUyLjE0MVY0OC4wMDl6IiBmaWxsPSIjRkZGRkZGIi8+CgkJPHBhdGggZD0iTTM4LjAxMyw0Ni4wNjFsMi45ODItMS43MTFjMC41MTMtMC4yOTQsMC41MTMtMC43NzIsMC0xLjA2NmwtMi45ODItMS43MWMtMC41MTUtMC4yOTUtMC45MzItMC4wNTQtMC45MzIsMC41Mzh2My40MSAgICBDMzcuMDgzLDQ2LjExMywzNy40OTgsNDYuMzU1LDM4LjAxMyw0Ni4wNjF6IiBmaWxsPSIjRkZGRkZGIi8+CgkJPHBhdGggZD0iTTQyLjUyMSw0Ni4wNjFsMi43MTItMS41NTd2MC45MzljMCwwLjU5MSwwLjMyNSwxLjA3MSwwLjcyNiwxLjA3MWMwLjQwMiwwLDAuNzI3LTAuNDc5LDAuNzI3LTEuMDcxdi0zLjI1MiAgICBjMC0wLjU5My0wLjMyNC0xLjA3Mi0wLjcyNy0xLjA3MmMtMC40LDAtMC43MjYsMC40NzktMC43MjYsMS4wNzJ2MC45MzlsLTIuNzEyLTEuNTU4Yy0wLjUxNC0wLjI5NS0wLjkzMS0wLjA1NC0wLjkzMSwwLjUzOHYzLjQxICAgIEM0MS41OTIsNDYuMTEzLDQyLjAwOCw0Ni4zNTUsNDIuNTIxLDQ2LjA2MXoiIGZpbGw9IiNGRkZGRkYiLz4KCQk8cGF0aCBkPSJNMjYuOTA2LDQ1LjgybDIuOTgyLTEuNzExYzAuNTEzLTAuMjk1LDAuNTEzLTAuNzcyLDAtMS4wNjVsLTIuOTgyLTEuNzEyYy0wLjUxNS0wLjI5NS0wLjkzMS0wLjA1My0wLjkzMSwwLjU0djMuNDA4ICAgIEMyNS45NzYsNDUuODczLDI2LjM5Miw0Ni4xMTUsMjYuOTA2LDQ1LjgyeiIgZmlsbD0iI0ZGRkZGRiIvPgoJCTxwYXRoIGQ9Ik0xNC43NTksNDQuMzVsMi45ODIsMS43MTFjMC41MTMsMC4yOTUsMC45MywwLjA1MywwLjkzLTAuNTM5di0zLjQwOWMwLTAuNTkxLTAuNDE3LTAuODMzLTAuOTMtMC41MzhsLTIuOTgyLDEuNzEgICAgQzE0LjI0Niw0My41NzgsMTQuMjQ2LDQ0LjA1NywxNC43NTksNDQuMzV6IiBmaWxsPSIjRkZGRkZGIi8+CgkJPHBhdGggZD0iTTkuNzkzLDQ2LjUxNWMwLjQwMSwwLDAuNzI3LTAuNDc5LDAuNzI3LTEuMDcxdi0wLjkzOWwyLjcxMywxLjU1N2MwLjUxNSwwLjI5NSwwLjkzLDAuMDUzLDAuOTMtMC41Mzl2LTMuNDA5ICAgIGMwLTAuNTkxLTAuNDE2LTAuODMzLTAuOTMtMC41MzhsLTIuNzEzLDEuNTU3di0wLjkzOGMwLTAuNTkyLTAuMzI1LTEuMDcxLTAuNzI3LTEuMDcxYy0wLjQwMSwwLTAuNzI3LDAuNDc5LTAuNzI3LDEuMDcxdjMuMjUyICAgIEM5LjA2Nyw0Ni4wMzQsOS4zOTMsNDYuNTE1LDkuNzkzLDQ2LjUxNXoiIGZpbGw9IiNGRkZGRkYiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K",
    //episodeTitle: '',
    errors: [],
   	typeError: false,
    title: "",
    titleSlug: "",
    serie: '',
    epNum: 0,
    audioLang: 'JAP',
    subLang: 'ITA',
    //dropForm: document.querySelector("#fileForm"),
    classicFile: document.querySelector("#classicUpload"),
    uploadPercentage: 0
  },
  mounted: function() {
  	// Temporaneo, mi serve per riempire il nome della serie con un qualcosa che dati non ne ho 	
  		this.serie = "nome-serie";
  		//this.file = "IL FILE";
  	

  	// Numero episodio casuale, sempre temporaneo 
  	this.epNum = Math.floor(Math.random() * (400 - 1)) + 1;

  	// End if - TEMPORARY
  	this.dragDropCapable = this.checkIfDragDrop();

  	if(this.dragDropCapable) { 

  		// Per ogni tipo di evento del Drag n Drop previeni il comportamento di default
  		// per esempio aprire il file nel browser o copiarlo in qualche elemento
  		// della pagina
  		/* PER IL MOMENTO, TOLGO TUTTA LA FUNZIONE DI DRAG N DROP PERCHE' TANTO
  		   SI UPPA UN FILE ALLA VOLTA 

  		['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach(function(event) {

  			this.dropForm.addEventListener(event, function(e) {
  				e.preventDefault();
  				e.stopPropagation();

  			}.bind(this), false); // end prevent function
  		}.bind(this)); // end forEach event function

  		// Drop event listener 

  		this.dropForm.addEventListener('drop', function(e) {

  			// File singolo
  			if(e.dataTransfer.files.length > 1) {
  				this.errors.push("Non è possibile caricare più di un file alla volta!");
  			} else { 

  			// Acchiappa i file droppati e inseriscili nei nostri dati
  			// IMPORTANTE! UPLOAD DI UN SOLO FILE! (ANCHE SE LA LOGICA INCLUDE FILE MULTIPLI, SENZA USARNE QUINDI.)

  				for(let i=0; i<e.dataTransfer.files.length;i++) {

  					// Inserisci i file droppati nella variabile files

  					this.files.push(e.dataTransfer.files[i]); 

  				} //end for

  			} // end if files.length > 1





  		}.bind(this)); // end drop event function

  		// Dragenter event listener

  		this.dropForm.addEventListener('dragenter', function(e) {

  			this.dropSpan = "Rilascia il file quì per aggiungerlo...";
  			this.isOnEnter = true;


  		}.bind(this)); // end dragenter function

  	} else { // if drag n drop is not available

  		this.file = this.classicUpload.files[0];

  	} //end if-else  FINE BLOCCO TOLTO DRAG N DROP */

  	//this.file = this.classicFile.files[0];
  	}


  	//console.log(window.location.toString().substr(window.location.toString().indexOf('/' + 1)));
  	//console.log("Tutto dovrebbe funzionare");
  }, // end mounted function

  methods: {

  	checkIfDragDrop: function() {

  		// Controlla se la funziona Drag n Drop è utilizzabile del browser dell'utente

  		return ('FormData' in window);
//(('draggable' in div) || ('ondragstart' in div && 'ondrop' in div))
  	}, // end checkIfDragDrop
  	validateData: function() {
  		if(!this.dragDropCapable) {
  			this.errors.push("C'è stato un errore con lo script, controlla di non aver ricevuto l'avvertimento che chiede di aggiornare il browser.");
  			return false;

  		}
  		if(this.typeError) {
  			this.errors.push("Il file inserito ( {{ file.name }} ) non è in un formato video valido, perciò non è stato caricato.");
  			return false;
  		}
  		if(this.title == '' || this.title == null){
  			this.errors.push("Il campo <strong>titolo</strong> è vuoto, perfavore inserisci un titolo per questo episodio.");
  			return false;
  		}
  		return true;

  	}, // end validateData function

  	sendFile: function() {
  		if(this.validateData()) {
  			let formData = new FormData();
  			formData.append('title', this.title);
  			formData.append('episode', this.epNum);
  			formData.append('audio', this.audioLang);
  			formData.append('sub', this.subLang);
        formData.append('video', this.file);

  			// Popolati i dati da mandare, manda una richiesta con axios

  			axios.post('http://127.0.0.1:9000', formData, // set endpoint and inser formData variable
  				{
  					header: { 'Content-Type':'multipart/form-data', 'Access-Control-Allow-Origin':'*' }, // set HTTP headers
  					onUploadProgress: function(progressEvent) {
  						// Calcola la percentuale da mostrare durante il caricamento
  						this.uploadPercentage = parseInt(Math.round( (progressEvent.loaded * 100) / progressEvent.total ) );
  					}.bind(this)
  				}).
  			then(function(res) { // Ricevi la risposta dall'endpoint
  				alert("Successo! " + res);

  			}).
  			catch(function(error) { // "Cattura" ogni possibile errore restituito dall'endpoint
  				alert("Errore : " + error);
  				console.log("Failure, error :" + error);

  			}); // end axios post request

  		} // end if validate


  		//console.log(formData);



  		// axios.post() .. .. here goes post


  	}, // end sendFile function

  	fileChanged: function(event) {

  		this.file = event.target.files[0];
  		this.title = this.file.name.substring(0, this.file.name.lastIndexOf('.')); // Togli l'estensione
  		this.size = this.file.size;
  		if(this.file.type != "video/mp4") {
  			this.typeError = true;
  		} else {
  			this.typeError = false;
  		} // end if else

      this.titleSlug = this.title.toString().toLowerCase()
      .replace(/[ÀÁÂÃÄÅ]/g,"A")
      .replace(/[àáâãäå]/g,"a")
      .replace(/[ÈÉÊË]/g,"E")
      .replace(/[èé]/g,"e")
      .replace(/[ù]/g,"u")
      .replace(/[ò]/g,"o")
      .replace(/[^\w\s-]/g, '')
      .replace(/[\s_-]+/g, '-')
      .replace(/^-+|-+$/g, '');





  	} // end fileChanged function


  }, // end methods
  computed: {

  	bytesToMb: function() {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (this.size == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(this.size) / Math.log(1024)));
    if (i == 0) return this.size + ' ' + sizes[i];
    return (this.size / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];

  	} //end bytesToMb function

  }, // end computed properties

  watch: {
  	title: function() {
  		
  		this.titleSlug = this.title.toString().toLowerCase()
  		.replace(/[ÀÁÂÃÄÅ]/g,"A")
  		.replace(/[àáâãäå]/g,"a")
  		.replace(/[ÈÉÊË]/g,"E")
  		.replace(/[èé]/g,"e")
  		.replace(/[ù]/g,"u")
  		.replace(/[ò]/g,"o")
  		.replace(/[^\w\s-]/g, '')
  		.replace(/[\s_-]+/g, '-')
  		.replace(/^-+|-+$/g, ''); 

  	} // end title watch function

  } // end watchers

}) // end Vue app