var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var notify = require("gulp-notify");
var rename = require("gulp-rename");
var watch = require('gulp-watch');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify');
var jsSources = ['src/js/vue.js', 'src/js/axios.js', 'src/js/main.js'];

gulp.task('css', function(){
  return gulp.src('src/css/main.scss')
    .pipe(sass().on("error", sass.logError))
    .pipe(minifyCSS())
    .pipe(rename("style.min.css")) 
    .pipe(gulp.dest('dist/css'))
});

gulp.task('js', function(){
  return gulp.src(jsSources)
    .pipe(sourcemaps.init())
    .pipe(concat('scripts.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/js'))
});


gulp.task('mini', function(){
  return gulp.src('dist/js/scripts.js')
    .pipe(minify({
        ext: {
            src: '-debug.js',
            min: '.min.js'
        }
    }))
    .pipe(gulp.dest('dist/js'))
});

gulp.task('default', [ 'css', 'js']);

//gulp.watch(['src/js/main.js', 'src/css/main.css'], ['default']);